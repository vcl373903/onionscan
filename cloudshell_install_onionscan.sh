#!/bin/bash

go install gitlab.com/vcl373903/onionscan@latest
cd gopath
cp -r `find ~+ -type d -name "templates"` $HOME
cd -
